<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Notification</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
    {{ include resource_path("views/emails/{$template}/themes/{$theme}.css") }}
    </style>
  </head>
  <body>
    <table id="main" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th>
            <img src="{{ url("emails/{$template}/schoolable.png") }}" alt="Schoolable" class="logo" />
          </th>
        </tr>
        @isset($titleImage)
        <tr>
          <th>
            <img src="{{ url("emails/{$template}/" . $titleImage) }}" alt="" class="title-image">
          </th>
        </tr>
        @endisset
      </thead>
      <tbody>
        <tr>
          <td id="content">
            @yield("content")
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td>
            <a href="https://facebook.com/allprohq">
              <img src="{{ url("emails/{$template}/facebook.svg") }}" alt="Facebook" class="social-logo">
            </a>
            <a href="https://linkedin.com/in/allprohq">
              <img src="{{ url("emails/{$template}/linkedIn.svg") }}" alt="LinkedIn" class="social-logo">
            </a>
            <a href="https://twittter.com/allprohq">
              <img src="{{ url("emails/{$template}/twitter.svg") }}" alt="Twitter" class="social-logo">
            </a>
          </td>
        </tr>
        <tr>
          <td id="address">
            J9A, Close 1, Road 3,<br> Victoria Garden City, Lagos Nigeria
          </td>
        </tr>
        {{-- <tr>
          <td>
            <p>
              <span class="unsub">
                You've recieved this email because you've signed up to recieve email updates from Schoolable
              </span>
            </p>
            <br />
            <a href="#" class="href">Unsubscribe</a>
          </td>
        </tr> --}}
      </tfoot>
    </table>
  </body>
</html>


