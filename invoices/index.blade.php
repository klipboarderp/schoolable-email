<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Invoice Notification</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
        {{ include resource_path('views/emails/invoices/themes/default.css') }}
        </style>
    </head>
    <body>
        <section class="container">
            <div class="container-body">
                <!-- left -->
                <div id="invoice-detail">
                    <div class="details">
                        <div class="name-title">
                            <div> 
                                <strong>Adegoke Damola</strong>
                            </div>
                            <div class="grey-text text-end">
                                12 Sep, 2018
                            </div>
                        </div>
                        <div class="line"></div>
                        <div>
                            <strong>The Cradle Schools</strong>
                            <p class="grey-text">
                                Primary 2A
                            </p>
                            <p class="pt-sm">
                                Invoice ID
                            </p>
                            <strong>32354432312</strong>
                            <p class="grey-text pt-sm">
                                Ebills ID
                            </p>
                            <strong>
                                32354432312
                            </strong>
                            <p class="hint grey-text pt-lg">
                                Scan the above QR code using your bank mobile App to make payments
                            </p>
                        </div>
                    </div>
                    <!-- right -->
                    <div class="qr-code">
                        <div class="title">
                            <strong>Scan to Pay</strong>
                            <img src="{{ App\Helper::imgURI(resource_path('views/emails/invoices/assets/qr-code.svg')) }}" alt="">
                        </div>
                        <div class="total-invoice">
                            <p>Total</p>
                            <p>$231,000,000</p>
                        </div>
                    </div>
                </div>
                <button class="pay-btn">Pay Online</button>
            </div>
        </section>
    </body>
</html>